CREATE TABLE IF NOT EXISTS cliente ( 
   ID BIGINT NOT NULL auto_increment, 
   nombre VARCHAR(30) NOT NULL, 
   apellido VARCHAR(30) NOT NULL,
   edad SMALLINT(5) NOT NULL,
   documento VARCHAR(30) NOT NULL,
   correo_electronico VARCHAR(60) NOT NULL,
   usuario VARCHAR(60) NOT NULL,
   clave VARCHAR(60) NOT NULL,
   PRIMARY KEY  (ID),
   UNIQUE KEY UK_F5XK6O6YDUNQ8D5C631T6EEP7_INDEX_5 (correo_electronico),
   UNIQUE KEY UK_G18G15IJPOFTFXM5V87AWPBYM_INDEX_5 (documento),
   UNIQUE KEY UK_G18G15IJPOFTFXM5V87AWPBYM_INDEX_5 (usuario)
);

CREATE TABLE IF NOT EXISTS tienda (
   ID BIGINT NOT NULL auto_increment,
   nombre VARCHAR(30) NOT NULL,
   direccion VARCHAR(255) NOT NULL, 
   horario VARCHAR(255) NOT NULL,
   PRIMARY KEY  (ID)
);

CREATE TABLE IF NOT EXISTS producto (
   ID BIGINT NOT NULL auto_increment, 
   nombre VARCHAR(60) NOT NULL, 
   descripcion VARCHAR(255) NOT NULL,
   precio DECIMAL(7, 2) NOT NULL,
   tienda_id BIGINT NOT NULL,
   codigo_barras VARCHAR(12) NOT NULL,
   PRIMARY KEY  (ID)
);

ALTER TABLE producto ADD FOREIGN KEY (tienda_id) REFERENCES tienda(ID);

CREATE TABLE IF NOT EXISTS compra (
   ID BIGINT NOT NULL auto_increment,
   fecha TIMESTAMP(23, 10) NOT NULL,
   precio_total DECIMAL(7, 2) NOT NULL, 
   cliente_id BIGINT NOT NULL,
   tienda_id BIGINT NOT NULL,
   PRIMARY KEY  (ID)
);

ALTER TABLE compra ADD FOREIGN KEY (cliente_id) REFERENCES cliente(ID);
ALTER TABLE compra ADD FOREIGN KEY (tienda_id) REFERENCES tienda(ID);

CREATE TABLE IF NOT EXISTS item_compra (
   ID BIGINT NOT NULL auto_increment,
   cantidad DECIMAL(7, 2) NOT NULL,
   compra_id BIGINT NOT NULL,
   tproducto_id BIGINT NOT NULL,
   PRIMARY KEY  (ID)
);

ALTER TABLE item_compra ADD FOREIGN KEY (compra_id) REFERENCES compra(ID);
ALTER TABLE item_compra ADD FOREIGN KEY (producto_id) REFERENCES producto(ID);