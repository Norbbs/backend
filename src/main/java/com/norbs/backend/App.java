package com.norbs.backend;

import com.norbs.backend.config.Swagger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication(scanBasePackages = {"com.norbs.backend.*"})
@Import(value = Swagger.class)
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}