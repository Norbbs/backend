package com.norbs.backend.domain.base;

import javax.persistence.*;

@MappedSuperclass
public abstract class EntidadBase {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;

        if (!(o instanceof EntidadBase)) {
            return false;
        }

        EntidadBase entidadBase = (EntidadBase) o;

        return this.id != null ? this.id.equals(entidadBase.id) : entidadBase.id == null;
    }

    @Override
    public int hashCode() {
        return this.id != null ? this.id.hashCode() : 0;
    }
}
