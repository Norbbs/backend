package com.norbs.backend.domain.compra;

import com.norbs.backend.domain.base.EntidadBase;
import com.norbs.backend.domain.tienda.Producto;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "item_compra")
public class ItemCompra extends EntidadBase {

    @ManyToOne
    @JoinColumn(name = "compra_id", nullable = false)
    private Compra compra;

    @ManyToOne
    @JoinColumn(name = "producto_id")
    private Producto producto;

    @Column(name = "cantidad", precision = 7, scale = 2, nullable = false)
    private BigDecimal cantidad;

    @Transient
    @Column(name = "precio", precision = 7, scale = 2, nullable = false)
    private BigDecimal precio;

    public Compra getCompra() {
        return compra;
    }

    public void setCompra(Compra compra) {
        this.compra = compra;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }
}
