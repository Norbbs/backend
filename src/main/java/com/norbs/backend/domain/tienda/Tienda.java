package com.norbs.backend.domain.tienda;

import com.norbs.backend.domain.base.EntidadBase;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "tienda")
public class Tienda extends EntidadBase {

    @Column(name = "nombre", length = 30, nullable = false)
    private String nombre;

    @Column(name = "horario", nullable = false)
    private String horario;

    @Column(name = "direccion", nullable = false)
    private String direccion;

    @OneToMany(mappedBy = "tienda")
    private List<Producto> productos;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }
}
