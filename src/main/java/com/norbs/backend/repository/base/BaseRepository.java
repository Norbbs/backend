package com.norbs.backend.repository.base;

import com.norbs.backend.domain.base.EntidadBase;
import com.norbs.backend.repository.classes.tables.records.*;
import org.jooq.DSLContext;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.conf.RenderKeywordStyle;
import org.jooq.conf.RenderNameStyle;
import org.jooq.conf.Settings;
import org.jooq.impl.DSL;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.DriverManager;
import java.sql.SQLException;

public abstract class BaseRepository<T extends EntidadBase> implements Repository<T> {

    private EntityManager entityManager;

    private DSLContext dslContext;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    protected EntityManager getEntityManager() {
        return entityManager;
    }

    protected DSLContext getDslContext() {

        if (dslContext == null) {
            try {
                dslContext = DSL.using(DriverManager.getConnection("jdbc:h2:~/test",
                        "sa", ""),
                        SQLDialect.H2,
                        new Settings()
                                .withRenderFormatted(true)
                                .withRenderNameStyle(RenderNameStyle.QUOTED)
                                .withRenderKeywordStyle(RenderKeywordStyle.UPPER)
                                .withRenderSchema(false));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return dslContext;
    }

    @Override
    @Transactional
    public void insertar(T entidad) {
        this.entityManager.persist(entidad);
    }

    @Override
    public T buscar(Class<T> clase, long id) {
        return this.entityManager.find(clase, id);
    }

    @Override
    @Transactional
    public void actualizar(T entidad) {
        this.entityManager.merge(entidad);
    }

    @Override
    @Transactional
    public void eliminar(T entidad) {
        this.entityManager.remove(entidad);
    }

    @Override
    public boolean existeCorreoElectronico(Long id, String correoElectronico) {
        throw new UnsupportedOperationException("Método boolean existeCorreoElectronico(Long id, String correoElectronico) no es soportado por esta clase.");
    }

    @Override
    public boolean existeDocumento(Long id, String documento) {
        throw new UnsupportedOperationException("Método boolean existeDocumento(Long id, String correoElectronico) no es soportado por esta clase.");
    }

    @Override
    public boolean existeNombreUsuario(Long id, String nombreUsuario) {
        throw new UnsupportedOperationException("Método boolean existeNombreUsuario(Long id, String nombreUsuario) no es soportado por esta clase.");
    }

    @Override
    public String obtenerClaveUsuario(String nombreUsuario) {
        throw new UnsupportedOperationException("Método String obtenerClaveUsuario(String nombreUsuario) no es soportado por esta clase.");
    }

    @Override
    public ClienteRecord iniciarSesion(String nombreUsuario, String clave) {
        throw new UnsupportedOperationException("Método ClienteRecord iniciarSesion(String nombreUsuario, String clave) no es soportado por esta clase.");
    }

    @Override
    public boolean existeProducto(Long tiendaID, String nombre) {
        throw new UnsupportedOperationException("Método boolean existeProducto(Long tiendaID, String nombre) no es soportado por esta clase.");
    }

    @Override
    public boolean existeCodigoDeBarras(Long id, String codigo) {
        throw new UnsupportedOperationException("Método boolean existeCodigoDeBarras(Long id, String codigo) no es soportado por esta clase.");
    }

    @Override
    public Result<ClienteRecord> buscarClientes() {
        throw new UnsupportedOperationException("Método Result<ClienteRecord> buscarClientes() no es soportado por esta clase.");
    }

    @Override
    public Result<TiendaRecord> buscarTiendas() {
        throw new UnsupportedOperationException("Método Result<TiendaRecord> buscarTiendas() no es soportado por esta clase.");
    }

    @Override
    public Result<ProductoRecord> buscarProductos() {
        throw new UnsupportedOperationException("Método Result<TiendaRecord> buscarProductos() no es soportado por esta clase.");
    }

    @Override
    public Result<ProductoRecord> buscarProductosPorTienda(long tiendaID) {
        throw new UnsupportedOperationException("Método Result<TiendaRecord> buscarProductosPorTienda(long tiendaID) no es soportado por esta clase.");
    }

    @Override
    public Result<CompraRecord> consultarComprasCliente(long clienteID) {
        throw new UnsupportedOperationException("Método Result<CompraRecord> consultarComprasCliente(long clienteID) no es soportado por esta clase.");
    }

    @Override
    public Result<ItemCompraRecord> consultarItemsCompra(long compraID) {
        throw new UnsupportedOperationException("Método Result<ItemCompraRecord> consultarItemsCompra(long compraID) no es soportado por esta clase.");
    }

    @Override
    public void eliminarComprasCliente(long clienteID) {
        throw new UnsupportedOperationException("Método void eliminarCompras(long compraID) no es soportado por esta clase.");
    }

    @Override
    public void eliminarComprasTienda(long tiendaID) {
        throw new UnsupportedOperationException("Método void eliminarComprasTienda(long tiendaID) no es soportado por esta clase.");
    }

    @Override
    public void eliminarComprasProducto(long productoID) {
        throw new UnsupportedOperationException("Método void eliminarComprasProducto(long productoID) no es soportado por esta clase.");
    }
}