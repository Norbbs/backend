package com.norbs.backend.repository.base;

import com.norbs.backend.repository.classes.tables.records.*;
import org.jooq.Result;

public interface Repository<EntityClass> {

    void insertar(EntityClass entidad);

    EntityClass buscar(Class<EntityClass> clase, long id);

    void actualizar(EntityClass entidad);

    void eliminar(EntityClass entidad);

    boolean existeCorreoElectronico(Long id, String correoElectronico);

    boolean existeDocumento(Long id, String documento);

    boolean existeNombreUsuario(Long id, String usuarip);

    String obtenerClaveUsuario(String nombreUsuario);

    ClienteRecord iniciarSesion(String nombreUsuario, String clave);

    Result<ClienteRecord> buscarClientes();

    boolean existeProducto(Long tiendaID, String nombre);

    boolean existeCodigoDeBarras(Long id, String codigo);

    Result<TiendaRecord> buscarTiendas();

    Result<ProductoRecord> buscarProductos();

    Result<ProductoRecord> buscarProductosPorTienda(long tiendaID);

    Result<CompraRecord> consultarComprasCliente(long clienteID);

    Result<ItemCompraRecord> consultarItemsCompra(long compraID);

    void eliminarComprasCliente(long clienteID);

    void eliminarComprasTienda(long tiendaID);

    void eliminarComprasProducto(long productoID);
}