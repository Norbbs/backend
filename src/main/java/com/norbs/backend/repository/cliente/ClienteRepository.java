package com.norbs.backend.repository.cliente;

import com.norbs.backend.domain.cliente.Cliente;
import com.norbs.backend.repository.base.BaseRepository;
import com.norbs.backend.repository.classes.Tables;
import com.norbs.backend.repository.classes.tables.records.ClienteRecord;
import org.jooq.Record1;
import org.jooq.Result;
import org.springframework.stereotype.Repository;

@Repository(value = "clienteRepository")
public class ClienteRepository extends BaseRepository<Cliente> {

    private static final com.norbs.backend.repository.classes.tables.Cliente T = Tables.CLIENTE.as("T");

    @Override
    public boolean existeCorreoElectronico(Long id, String correoElectronico) {
        return this.getDslContext()
                .fetchExists(this.getDslContext()
                        .selectOne()
                        .from(T)
                        .where((id != null && id > 0) ? T.CORREO_ELECTRONICO.eq(correoElectronico).and(T.ID.notEqual(id)) : T.CORREO_ELECTRONICO.eq(correoElectronico)));
    }

    @Override
    public boolean existeDocumento(Long id, String documento) {
        return this.getDslContext()
                .fetchExists(this.getDslContext()
                        .selectOne()
                        .from(T)
                        .where((id != null && id > 0) ? T.DOCUMENTO.eq(documento).and(T.ID.notEqual(id)) : T.DOCUMENTO.eq(documento)));
    }

    @Override
    public boolean existeNombreUsuario(Long id, String nombreUsuario) {
        return this.getDslContext()
                .fetchExists(this.getDslContext()
                        .selectOne()
                        .from(T)
                        .where((id != null && id > 0) ? T.USUARIO.eq(nombreUsuario).and(T.ID.notEqual(id)) : T.USUARIO.eq(nombreUsuario)));
    }

    @Override
    public String obtenerClaveUsuario(String nombreUsuario) {
        Record1<String> resultado =  this.getDslContext()
                .select(T.CLAVE)
                .from(T)
                .where(T.USUARIO.eq(nombreUsuario))
                .fetchOne();

        return resultado == null ? null : resultado.value1();
    }

    @Override
    public ClienteRecord iniciarSesion(String nombreUsuario, String clave) {
        return this.getDslContext()
                .selectFrom(T)
                .where(T.USUARIO.eq(nombreUsuario).and(T.CLAVE.eq(clave)))
                .fetchOne();
    }

    @Override
    public Result<ClienteRecord> buscarClientes() {
        return this.getDslContext()
                .selectFrom(T)
                .orderBy(T.NOMBRE.asc())
                .fetch();
    }
}