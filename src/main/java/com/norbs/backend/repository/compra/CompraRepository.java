package com.norbs.backend.repository.compra;

import com.norbs.backend.domain.compra.Compra;
import com.norbs.backend.repository.base.BaseRepository;
import com.norbs.backend.repository.classes.Tables;
import com.norbs.backend.repository.classes.tables.records.CompraRecord;
import org.jooq.Result;
import org.springframework.stereotype.Repository;

@Repository(value = "compraRepository")
public class CompraRepository extends BaseRepository<Compra> {

    private static final com.norbs.backend.repository.classes.tables.Compra C = Tables.COMPRA.as("T");

    private static final com.norbs.backend.repository.classes.tables.ItemCompra IC = Tables.ITEM_COMPRA.as("IC");

    @Override
    public Result<CompraRecord> consultarComprasCliente(long clienteID) {
        return this.getDslContext()
                .selectFrom(C)
                .where(C.CLIENTE_ID.eq(clienteID))
                .orderBy(C.ID.desc())
                .fetch();
    }

    @Override
    public void eliminarComprasCliente(long clienteID) {

        // Borrando items compra...
        this.getDslContext()
                .deleteFrom(IC)
                .where(IC.COMPRA_ID.in(this.getDslContext()
                                        .select(C.ID)
                                        .from(C)
                                        .where(C.CLIENTE_ID.eq(clienteID))
                                        .fields()));

        // Borrando compras...
        this.getDslContext()
                .deleteFrom(C)
                .where(C.CLIENTE_ID.eq(clienteID));
    }

    @Override
    public void eliminarComprasTienda(long tiendaID) {

        // Borrando items compra...
        this.getDslContext()
                .deleteFrom(IC)
                .where(IC.COMPRA_ID.in(this.getDslContext()
                        .select(C.ID)
                        .from(C)
                        .where(C.TIENDA_ID.eq(tiendaID))
                        .fields()));

        // Borrando compras...
        this.getDslContext()
                .deleteFrom(C)
                .where(C.TIENDA_ID.eq(tiendaID));
    }

    @Override
    public void eliminarComprasProducto(long productoID) {

        // Borrando items compra...
        this.getDslContext()
                .deleteFrom(IC)
                .where(IC.PRODUCTO_ID.eq(productoID));
    }
}