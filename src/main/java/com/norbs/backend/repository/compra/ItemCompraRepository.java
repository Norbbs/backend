package com.norbs.backend.repository.compra;

import com.norbs.backend.domain.compra.ItemCompra;
import com.norbs.backend.repository.base.BaseRepository;
import com.norbs.backend.repository.classes.Tables;
import com.norbs.backend.repository.classes.tables.records.ItemCompraRecord;
import org.jooq.Result;
import org.springframework.stereotype.Repository;

@Repository(value = "itemCompraRepository")
public class ItemCompraRepository extends BaseRepository<ItemCompra> {

    private static final com.norbs.backend.repository.classes.tables.ItemCompra T = Tables.ITEM_COMPRA.as("T");

    @Override
    public Result<ItemCompraRecord> consultarItemsCompra(long compraID) {
        return this.getDslContext()
                .selectFrom(T)
                .where(T.COMPRA_ID.eq(compraID))
                .orderBy(T.ID.desc())
                .fetch();
    }
}