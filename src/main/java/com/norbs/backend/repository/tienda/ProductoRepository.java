package com.norbs.backend.repository.tienda;

import com.norbs.backend.domain.tienda.Producto;
import com.norbs.backend.repository.base.BaseRepository;
import com.norbs.backend.repository.classes.Tables;
import com.norbs.backend.repository.classes.tables.records.ProductoRecord;
import org.jooq.Result;
import org.springframework.stereotype.Repository;

@Repository(value = "productoRepository")
public class ProductoRepository extends BaseRepository<Producto> {

    private static final com.norbs.backend.repository.classes.tables.Producto T = Tables.PRODUCTO.as("T");

    @Override
    public boolean existeCodigoDeBarras(Long id, String codigo) {
        return this.getDslContext()
                .fetchExists(this.getDslContext()
                        .selectOne()
                        .from(T)
                        .where((id != null && id > 0) ? T.CODIGO_BARRAS.eq(codigo).and(T.ID.notEqual(id)) : T.CODIGO_BARRAS.eq(codigo)));
    }

    @Override
    public boolean existeProducto(Long tiendaID, String nombre) {
        return this.getDslContext()
                .fetchExists(this.getDslContext()
                        .selectOne()
                        .from(T)
                        .where(T.TIENDA_ID.eq(tiendaID).and(T.NOMBRE.eq(nombre))));
    }

    @Override
    public Result<ProductoRecord> buscarProductos() {
        return this.getDslContext()
                .selectFrom(T)
                .orderBy(T.NOMBRE.asc())
                .fetch();
    }

    @Override
    public Result<ProductoRecord> buscarProductosPorTienda(long tiendaID) {
        return this.getDslContext()
                .selectFrom(T)
                .where(T.TIENDA_ID.eq(tiendaID))
                .orderBy(T.NOMBRE.asc())
                .fetch();
    }
}