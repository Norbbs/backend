package com.norbs.backend.repository.tienda;

import com.norbs.backend.domain.tienda.Tienda;
import com.norbs.backend.repository.base.BaseRepository;
import com.norbs.backend.repository.classes.Tables;
import com.norbs.backend.repository.classes.tables.records.TiendaRecord;
import org.jooq.Result;
import org.springframework.stereotype.Repository;

@Repository(value = "tiendaRepository")
public class TiendaRepository extends BaseRepository<Tienda> {

    private static final com.norbs.backend.repository.classes.tables.Tienda T = Tables.TIENDA.as("T");

    @Override
    public Result<TiendaRecord> buscarTiendas() {
        return this.getDslContext()
                .selectFrom(T)
                .orderBy(T.NOMBRE.asc())
                .fetch();
    }

}