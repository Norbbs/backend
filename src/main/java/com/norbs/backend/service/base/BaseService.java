package com.norbs.backend.service.base;

import com.norbs.backend.domain.base.EntidadBase;
import com.norbs.backend.repository.base.Repository;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseService<T extends EntidadBase> implements Service<T> {

    @Autowired
    private Repository<T> tRepository;

    @Override
    public void insertar(T entidad) throws Exception {
        this.tRepository.insertar(entidad);
    }

    @Override
    public T buscar(Class<T> clase, long id) {
        return this.tRepository.buscar(clase, id);
    }

    @Override
    public void actualizar(T entidad) throws Exception {
        this.tRepository.actualizar(entidad);
    }

    @Override
    public void eliminar(T entidad) throws Exception {
        this.tRepository.eliminar(entidad);
    }

    @Override
    public void validar(T entidad) throws Exception {
        if (entidad == null) {
            throw new Exception("Entidad inválida.");
        }

        if (entidad.getId() != null) {

            if (entidad.getId() > 0) {
                T entidadActual = this.buscar((Class<T>) entidad.getClass(), entidad.getId());

                if (entidadActual == null) {
                    throw new Exception("La entidad no existe o ha sido anulada.");
                }
            } else {
                throw new Exception("Error: el ID debe ser nulo o mayor a cero.");
            }
        }
    }
}