package com.norbs.backend.service.base;

public interface Service<EntityClass> {

    void insertar(EntityClass entidad) throws Exception;

    EntityClass buscar(Class<EntityClass> clase, long id);

    void actualizar(EntityClass entidad) throws Exception;

    void eliminar(EntityClass entidad) throws Exception;

    void validar(EntityClass entidad) throws Exception;
}