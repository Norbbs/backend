package com.norbs.backend.service.cliente;

import com.norbs.backend.domain.cliente.Cliente;
import com.norbs.backend.webservice.resources.ClienteJSON;

import java.util.List;

public interface ClienteService {

    void insertar(Cliente entidad) throws Exception;

    Cliente buscar(long id);

    void actualizar(Cliente entidad) throws Exception;

    void eliminar(long id) throws Exception;

    ClienteJSON iniciarSesion(String correoElectronico, String clave) throws Exception;

    List<ClienteJSON> buscar();
}