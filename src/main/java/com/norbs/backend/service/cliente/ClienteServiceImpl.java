package com.norbs.backend.service.cliente;

import com.norbs.backend.domain.cliente.Cliente;
import com.norbs.backend.repository.base.Repository;
import com.norbs.backend.repository.classes.tables.records.ClienteRecord;
import com.norbs.backend.service.base.BaseService;
import com.norbs.backend.service.compra.CompraService;
import com.norbs.backend.service.util.Util;
import com.norbs.backend.webservice.resources.ClienteJSON;
import org.jooq.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClienteServiceImpl extends BaseService<Cliente> implements ClienteService {

    @Autowired
    private Repository<Cliente> clienteRepository;

    @Autowired
    private CompraService compraService;

    @Override
    public void insertar(Cliente entidad) throws Exception {
        this.validar(entidad);

        entidad.setClave(this.cifrarClave(entidad.getClave()));

        this.clienteRepository.insertar(entidad);
    }

    @Override
    public Cliente buscar(long id) {
        return super.buscar(Cliente.class, id);
    }

    @Override
    public void actualizar(Cliente entidad) throws Exception {
        this.validar(entidad);

        this.clienteRepository.actualizar(entidad);
    }

    @Override
    public void eliminar(long id) throws Exception {

        Cliente cliente = this.buscar(id);

        if (cliente == null) {
            throw new Exception("El cliente indicado no existe");
        }

        this.compraService.eliminarComprasCliente(id);

        this.clienteRepository.eliminar(cliente);
    }

    @Override
    public ClienteJSON iniciarSesion(String nombreUsuario, String clave) throws Exception {
        if (Util.esNulaOVacia(nombreUsuario)) {
            throw new Exception("Ingresa tu nombre de usuario.");
        }

        if (Util.esNulaOVacia(clave)) {
            throw new Exception("Ingresa tu contraseña.");
        }

        String claveCifrada = this.clienteRepository.obtenerClaveUsuario(nombreUsuario);
        if (!this.esClaveCorrecta(clave, claveCifrada)) {
            throw new Exception("Nombre de usuario o contraseña incorrecta.");
        }

        return this.convertirAJSON(this.clienteRepository.iniciarSesion(nombreUsuario, claveCifrada));
    }

    @Override
    public List<ClienteJSON> buscar() {
        List<ClienteJSON> clientes = new ArrayList<>();
        Result<ClienteRecord> result = this.clienteRepository.buscarClientes();

        if (result != null) {
            for (ClienteRecord clienteRecord : result) {
                clientes.add(this.convertirAJSON(clienteRecord));
            }
        }

        return clientes;
    }

    @Override
    public void validar(Cliente entidad) throws Exception {
        super.validar(entidad);

        if (Util.esNulaOVacia(entidad.getNombre())) {
            throw new Exception("Ingresa tu nombre.");
        }

        if (Util.esNulaOVacia(entidad.getApellido())) {
            throw new Exception("Ingresa tu apellido.");
        }

        if (!Util.esEmailValido(entidad.getCorreoElectronico())) {
            throw new Exception("Ingresa una dirección de correo eletrónico válida.");
        }

        if (this.clienteRepository.existeCorreoElectronico(entidad.getId(), entidad.getCorreoElectronico())) {
            throw new Exception("El correo electrónico indicado ya existe.");
        }

        if (Util.esNulaOVacia(entidad.getDocumento())) {
            throw new Exception("Ingresa tu documento de identidad.");
        }

        if (this.clienteRepository.existeDocumento(entidad.getId(), entidad.getDocumento())) {
            throw new Exception("El documento indicado ya existe.");
        }

        if (entidad.getEdad() <= 0) {
            throw new Exception("Indica tu edad.");
        }

        if (Util.esNulaOVacia(entidad.getUsuario())) {
            throw new Exception("Ingresa tu nombre de usuario.");
        }

        if (this.clienteRepository.existeNombreUsuario(entidad.getId(), entidad.getUsuario())) {
            throw new Exception("El nombre de usuario indicado ya existe.");
        }

        if (Util.esNulaOVacia(entidad.getClave())) {
            throw new Exception("Ingresa tu clave de usuario.");
        }

        if (entidad.getClave().length() < 6) {
            throw new Exception("Su clave debe contener mínimo 6 caracteres.");
        }
    }

    private String cifrarClave(String claveNoCifrada) {
        return new BCryptPasswordEncoder().encode(claveNoCifrada);
    }

    private boolean esClaveCorrecta(String clave, String claveCifrada) {
        return new BCryptPasswordEncoder().matches(clave, claveCifrada);
    }

    private ClienteJSON convertirAJSON(ClienteRecord clienteRecord) {
        ClienteJSON cliente = new ClienteJSON();
        cliente.setID(clienteRecord.getId());
        cliente.setNombre(clienteRecord.getNombre());
        cliente.setApellido(clienteRecord.getApellido());
        cliente.setEdad(clienteRecord.getEdad());
        cliente.setDocumento(clienteRecord.getDocumento());
        cliente.setCorreoElectronico(clienteRecord.getCorreoElectronico());
        cliente.setUsuario(clienteRecord.getUsuario());
        cliente.setClave(clienteRecord.getClave());
        return cliente;
    }
}