package com.norbs.backend.service.compra;

import com.norbs.backend.webservice.resources.CarritoJSON;
import com.norbs.backend.webservice.resources.CompraJSON;

import java.util.List;

public interface CompraService {

    CompraJSON procesarCompra(CarritoJSON carritoCompras) throws Exception;

    List<CompraJSON> consultarComprasCliente(long clienteID);

    void eliminarComprasCliente(long clienteID);

    void eliminarComprasTienda(long tiendaID);

    void eliminarComprasProducto(long productoID);
}
