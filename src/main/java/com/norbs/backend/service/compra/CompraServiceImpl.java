package com.norbs.backend.service.compra;

import com.norbs.backend.domain.cliente.Cliente;
import com.norbs.backend.domain.compra.Compra;
import com.norbs.backend.domain.compra.ItemCompra;
import com.norbs.backend.domain.tienda.Tienda;
import com.norbs.backend.repository.base.Repository;
import com.norbs.backend.repository.classes.tables.records.CompraRecord;
import com.norbs.backend.service.base.BaseService;
import com.norbs.backend.service.cliente.ClienteService;
import com.norbs.backend.service.tienda.ProductoService;
import com.norbs.backend.service.tienda.TiendaService;
import com.norbs.backend.service.util.Util;
import com.norbs.backend.webservice.resources.CarritoJSON;
import com.norbs.backend.webservice.resources.CompraJSON;
import com.norbs.backend.webservice.resources.ItemCarritoJSON;
import com.norbs.backend.webservice.resources.ItemCompraJSON;
import org.jooq.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
@Transactional
public class CompraServiceImpl extends BaseService<Compra> implements CompraService {

    @Autowired
    private Repository<Compra> compraRepository;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private TiendaService tiendaService;

    @Autowired
    private ItemCompraService itemCompraService;

    @Autowired
    private ProductoService productoService;

    @Override
    @Transactional
    public CompraJSON procesarCompra(CarritoJSON carritoCompras) throws Exception {

        if (carritoCompras == null || Util.esNulaOVacia(carritoCompras.getItems())) {
            throw new Exception("Carrito vacío.");
        }

        if (carritoCompras.getClienteID() == null) {
            throw new Exception("Información del cliente es incorrecta.");
        }

        Cliente cliente = this.clienteService.buscar(carritoCompras.getClienteID());

        if (cliente == null) {
            throw new Exception("El cliente indicado no existe.");
        }

        List<ItemCompra> items = this.generarItemsCompra(carritoCompras.getItems());

        this.verificarItems(items);

        Compra compra = new Compra();
        compra.setFecha(new Date());
        compra.setCliente(cliente);
        compra.setTienda(items.get(0).getProducto().getTienda());
        compra.setPrecioTotal(this.calcularPrecioTotalCompra(items));
        compra.setItems(items);
        this.compraRepository.insertar(compra);

        this.insertarItems(compra);

        return this.convertirAJSON(compra);
    }

    @Override
    public List<CompraJSON> consultarComprasCliente(long clienteID) {
        List<CompraJSON> comprasJSON = new ArrayList<>();

        Result<CompraRecord> result = this.compraRepository.consultarComprasCliente(clienteID);

        if (result != null) {
            for (CompraRecord record : result) {
                comprasJSON.add(this.convertirAJSON(record));
            }
        }

        return comprasJSON;
    }

    @Override
    public void eliminarComprasCliente(long clienteID) {
        this.compraRepository.eliminarComprasCliente(clienteID);
    }

    @Override
    public void eliminarComprasTienda(long tiendaID) {
        this.compraRepository.eliminarComprasTienda(tiendaID);
    }

    @Override
    public void eliminarComprasProducto(long productoID) {
        this.compraRepository.eliminarComprasProducto(productoID);
    }

    private List<ItemCompra> generarItemsCompra(List<ItemCarritoJSON> items) {
        List<ItemCompra> itemCompras = new ArrayList<>();

        for (ItemCarritoJSON itemCarritoJSON : items) {
            ItemCompra itemCompra = new ItemCompra();
            itemCompra.setCantidad(itemCarritoJSON.getCantidad());
            itemCompra.setProducto(this.productoService.buscar(itemCarritoJSON.getProductoID()));
            itemCompra.setPrecio(Util.multiplicar(itemCompra.getProducto().getPrecio(), itemCompra.getCantidad().intValue()));
            itemCompras.add(itemCompra);
        }

        return itemCompras;
    }

    private CompraJSON convertirAJSON(CompraRecord compraRecord) {
        CompraJSON compraJSON = new CompraJSON();
        compraJSON.setID(compraRecord.getId());
        Cliente cliente = this.clienteService.buscar(compraRecord.getClienteId());
        compraJSON.setNombreCliente(cliente.getNombre() + " " + cliente.getApellido());
        compraJSON.setDocumentoCliente(cliente.getDocumento());
        Tienda tienda = this.tiendaService.buscar(compraRecord.getTiendaId());
        compraJSON.setNombreTienda(tienda.getNombre());
        compraJSON.setFecha(Util.obtenerFechaFormateada(compraRecord.getFecha()));
        compraJSON.setPrecioTotal(compraRecord.getPrecioTotal());
        compraJSON.setItems(this.itemCompraService.consultarItemsCompra(compraJSON.getID()));
        return compraJSON;
    }

    private CompraJSON convertirAJSON(Compra compra) {
        CompraJSON compraJSON = new CompraJSON();
        compraJSON.setID(compra.getId());
        compraJSON.setNombreCliente(compra.getCliente().getNombre() + " " + compra.getCliente().getApellido());
        compraJSON.setDocumentoCliente(compra.getCliente().getDocumento());
        compraJSON.setNombreTienda(compra.getTienda().getNombre());
        compraJSON.setFecha(Util.obtenerFechaFormateada(compra.getFecha()));
        compraJSON.setPrecioTotal(compra.getPrecioTotal());
        compraJSON.setItems(new ArrayList<>());

        for (ItemCompra itemCompra : compra.getItems()) {
            ItemCompraJSON itemCompraJSON = new ItemCompraJSON();
            itemCompraJSON.setID(itemCompra.getId());
            itemCompraJSON.setCompraID(itemCompra.getCompra().getId());
            itemCompraJSON.setNombreProducto(itemCompra.getProducto().getNombre());
            itemCompraJSON.setCantidad(itemCompra.getCantidad());
            itemCompraJSON.setPrecioUnitario(itemCompra.getProducto().getPrecio());
            itemCompraJSON.setPrecioTotal(itemCompra.getPrecio());
            compraJSON.getItems().add(itemCompraJSON);
        }
        return compraJSON;
    }

    private boolean sonProductosMismaTienda(List<ItemCompra> items) {

        if (items.size() == 1) {
            return true;
        }

        Long tiendaID = items.get(0).getProducto().getTienda().getId();

        for (ItemCompra itemCompra : items) {

            if (!Objects.equals(itemCompra.getProducto().getTienda().getId(), tiendaID)) {
                return false;
            }
        }

        return true;
    }

    private BigDecimal calcularPrecioTotalCompra(List<ItemCompra> items) {
        BigDecimal total = BigDecimal.ZERO;

        for (ItemCompra itemCompra : items) {
            total = Util.sumar(total, itemCompra.getPrecio());
        }

        return total;
    }

    private void verificarItems(List<ItemCompra> items) throws Exception {

        if (!this.sonProductosMismaTienda(items)) {
            throw new Exception("Los productos deben ser de la misma tienda");
        }

        for (ItemCompra item : items) {
            if (item.getProducto() == null) {
                throw new Exception("Debe indicar el producto a comprar.");
            }

            if (item.getCantidad() == null || Util.esMenorOIgual(item.getCantidad(), BigDecimal.ZERO)) {
                throw new Exception("La cantidad a comprar debe ser mayor a cero.");
            }

            if (item.getPrecio() == null || Util.esMenorOIgual(item.getPrecio(), BigDecimal.ZERO)) {
                throw new Exception("El precio debe ser mayor a cero.");
            }
        }

    }

    private void insertarItems(Compra compra) throws Exception {
        for (ItemCompra itemCompra : compra.getItems()) {
            itemCompra.setCompra(compra);
            this.itemCompraService.insertar(itemCompra);
        }
    }
}