package com.norbs.backend.service.compra;

import com.norbs.backend.domain.compra.ItemCompra;
import com.norbs.backend.webservice.resources.ItemCompraJSON;

import java.util.List;

public interface ItemCompraService {

    void insertar(ItemCompra entidad) throws Exception;

    List<ItemCompraJSON> consultarItemsCompra(long compraID);

    void validar(ItemCompra entidad) throws Exception;
}
