package com.norbs.backend.service.compra;

import com.norbs.backend.domain.compra.ItemCompra;
import com.norbs.backend.domain.tienda.Producto;
import com.norbs.backend.repository.base.Repository;
import com.norbs.backend.repository.classes.tables.records.ItemCompraRecord;
import com.norbs.backend.service.base.BaseService;
import com.norbs.backend.service.tienda.ProductoService;
import com.norbs.backend.service.util.Util;
import com.norbs.backend.webservice.resources.ItemCompraJSON;
import org.jooq.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ItemCompraServiceImpl extends BaseService<ItemCompra> implements ItemCompraService {

    @Autowired
    private Repository<ItemCompra> itemCompraRepository;

    @Autowired
    private ProductoService productoService;

    @Override
    @Transactional
    public void insertar(ItemCompra entidad) throws Exception {
        this.validar(entidad);

        this.itemCompraRepository.insertar(entidad);
    }

    @Override
    public List<ItemCompraJSON> consultarItemsCompra(long compraID) {
        List<ItemCompraJSON> items = new ArrayList<>();
        Result<ItemCompraRecord> itemCompraRecord = itemCompraRepository.consultarItemsCompra(compraID);

        if (itemCompraRecord != null) {
            for (ItemCompraRecord record : itemCompraRecord) {
                items.add(this.convertirAJSON(record));
            }
        }

        return items;
    }

    @Override
    public void validar(ItemCompra entidad) throws Exception {

        super.validar(entidad);

        if (entidad.getProducto() == null) {
            throw new Exception("Debe indicar el producto a comprar.");
        }

        if (entidad.getCantidad() == null || Util.esMenorOIgual(entidad.getCantidad(), BigDecimal.ZERO)) {
            throw new Exception("La cantidad a comprar debe ser mayor a cero.");
        }

        if (entidad.getCompra() == null) {
            throw new Exception("Compra no asignada");
        }

        if (entidad.getPrecio() == null || Util.esMenorOIgual(entidad.getPrecio(), BigDecimal.ZERO)) {
            throw new Exception("El precio debe ser mayor a cero.");
        }
    }

    private ItemCompraJSON convertirAJSON(ItemCompraRecord record) {
        ItemCompraJSON item = new ItemCompraJSON();
        item.setID(record.getId());
        item.setCompraID(record.getCompraId());
        Producto producto = this.productoService.buscar(record.getProductoId());
        item.setNombreProducto(producto.getNombre());
        item.setPrecioUnitario(producto.getPrecio());
        item.setCantidad(record.getCantidad());
        item.setPrecioTotal(Util.multiplicar(item.getPrecioUnitario(), item.getCantidad().intValue()));
        return item;
    }
}
