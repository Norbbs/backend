package com.norbs.backend.service.tienda;

import com.norbs.backend.domain.tienda.Producto;
import com.norbs.backend.webservice.resources.ProductoJSON;

import java.util.List;

public interface ProductoService {

    void insertar(Producto entidad) throws Exception;

    void insertar(ProductoJSON productoJSON) throws Exception;

    Producto buscar(long id);

    ProductoJSON buscarJSON(long id);

    void actualizar(Producto entidad) throws Exception;

    void actualizar(ProductoJSON productoJSON) throws Exception;

    void eliminar(long id) throws Exception;

    void eliminar(Producto entidad) throws Exception;

    List<ProductoJSON> buscar();

    List<ProductoJSON> buscarPorTienda(long tiendaID);

    void validar(Producto entidad) throws Exception;
}