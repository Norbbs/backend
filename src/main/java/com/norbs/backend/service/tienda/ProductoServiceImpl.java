package com.norbs.backend.service.tienda;

import com.norbs.backend.domain.tienda.Producto;
import com.norbs.backend.repository.base.Repository;
import com.norbs.backend.repository.classes.tables.records.ProductoRecord;
import com.norbs.backend.service.base.BaseService;
import com.norbs.backend.service.compra.CompraService;
import com.norbs.backend.service.util.Util;
import com.norbs.backend.webservice.resources.ProductoJSON;
import org.jooq.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ProductoServiceImpl extends BaseService<Producto> implements ProductoService {

    @Autowired
    private Repository<Producto> productoRepository;

    @Autowired
    private TiendaService tiendaService;

    @Autowired
    private CompraService compraService;

    @Override
    @Transactional
    public void insertar(Producto entidad) throws Exception {
        this.validar(entidad);

        this.productoRepository.insertar(entidad);
    }

    @Override
    @Transactional
    public void insertar(ProductoJSON productoJSON) throws Exception {
        Producto entidad = this.obtenerEntidadPorJSON(productoJSON);

        this.insertar(entidad);

        productoJSON.setID(entidad.getId());
    }

    @Override
    public Producto buscar(long id) {
        return super.buscar(Producto.class, id);
    }

    @Override
    public ProductoJSON buscarJSON(long id) {

        ProductoJSON productoJSON = new ProductoJSON();
        Producto producto = this.buscar(id);

        if (producto != null) {
            productoJSON.setID(producto.getId());
            productoJSON.setNombre(producto.getNombre());
            productoJSON.setDescripcion(producto.getDescripcion());
            productoJSON.setTiendaID(producto.getTienda().getId());
            productoJSON.setPrecio(producto.getPrecio());
            productoJSON.setCodigoBarras(producto.getCodigoBarras());
        }

        return productoJSON;
    }

    @Override
    @Transactional
    public void actualizar(Producto entidad) throws Exception {
        this.validar(entidad);

        this.productoRepository.actualizar(entidad);
    }

    @Override
    @Transactional
    public void actualizar(ProductoJSON productoJSON) throws Exception {
        this.actualizar(this.obtenerEntidadPorJSON(productoJSON));
    }

    @Override
    @Transactional
    public void eliminar(long id) throws Exception {
        Producto producto = this.buscar(id);

        if (producto == null) {
            throw new Exception("El producto indicado no existe.");
        }

        this.compraService.eliminarComprasProducto(id);

        this.eliminar(producto);
    }

    @Override
    @Transactional
    public void eliminar(Producto entidad) throws Exception {
        this.compraService.eliminarComprasProducto(entidad.getId());

        this.productoRepository.eliminar(entidad);
    }

    @Override
    public List<ProductoJSON> buscar() {
        List<ProductoJSON> tiendas = new ArrayList<>();
        Result<ProductoRecord> result = this.productoRepository.buscarProductos();

        if (result != null) {
            for (ProductoRecord productoRecord : result) {
                tiendas.add(this.convertirAJSON(productoRecord));
            }
        }

        return tiendas;
    }

    @Override
    public List<ProductoJSON> buscarPorTienda(long tiendaID) {
        List<ProductoJSON> tiendas = new ArrayList<>();
        Result<ProductoRecord> result = this.productoRepository.buscarProductosPorTienda(tiendaID);

        if (result != null) {
            for (ProductoRecord productoRecord : result) {
                tiendas.add(this.convertirAJSON(productoRecord));
            }
        }

        return tiendas;
    }

    @Override
    public void validar(Producto entidad) throws Exception {
        super.validar(entidad);

        if (entidad.getTienda() == null) {
            throw new Exception("Indica la tienda a la que pertenece el producto.");
        }

        if (Util.esNulaOVacia(entidad.getNombre())) {
            throw new Exception("Ingresa el nombre del producto.");
        }

        if (this.productoRepository.existeProducto(entidad.getTienda().getId(), entidad.getNombre())) {
            throw new Exception("El producto ya existe en la tienda indicada.");
        }

        if (Util.esNulaOVacia(entidad.getDescripcion())) {
            throw new Exception("Ingresa la descripción del producto.");
        }

        if (Util.esMenorOIgual(entidad.getPrecio(), BigDecimal.ZERO)) {
            throw new Exception("El precio del producto debe ser mayor a cero.");
        }

        if (Util.esNulaOVacia(entidad.getCodigoBarras())) {
            throw new Exception("Indica el código de barras.");
        }

        if (entidad.getCodigoBarras().length() > 12) {
            throw new Exception("El código de barras indicado no puede tener más de 12 dígitos.");
        }

        if (this.productoRepository.existeCodigoDeBarras(entidad.getId(), entidad.getCodigoBarras())) {
            throw new Exception("El código de barras indicado ya existe.");
        }
    }

    private ProductoJSON convertirAJSON(ProductoRecord productoRecord) {
        ProductoJSON productoJSON = new ProductoJSON();
        productoJSON.setID(productoRecord.getId());
        productoJSON.setTiendaID(productoRecord.getTiendaId());
        productoJSON.setNombre(productoRecord.getNombre());
        productoJSON.setDescripcion(productoRecord.getDescripcion());
        productoJSON.setPrecio(productoRecord.getPrecio());
        productoJSON.setCodigoBarras(productoRecord.getCodigoBarras());
        return productoJSON;
    }

    private Producto obtenerEntidadPorJSON(ProductoJSON productoJSON) {
        Producto producto = (productoJSON.getID() == null || productoJSON.getID() <= 0) ? new Producto() : this.buscar(productoJSON.getTiendaID());
        producto.setTienda(this.tiendaService.buscar(productoJSON.getTiendaID()));
        producto.setPrecio(productoJSON.getPrecio());
        producto.setNombre(productoJSON.getNombre());
        producto.setDescripcion(productoJSON.getDescripcion());
        producto.setPrecio(productoJSON.getPrecio());
        producto.setCodigoBarras(productoJSON.getCodigoBarras());
        return producto;
    }
}