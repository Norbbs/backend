package com.norbs.backend.service.tienda;

import com.norbs.backend.domain.tienda.Tienda;
import com.norbs.backend.webservice.resources.TiendaJSON;

import java.util.List;

public interface TiendaService {

    void insertar(TiendaJSON tiendaJSON) throws Exception;

    Tienda buscar(long id);

    TiendaJSON buscarJSON(long id);

    void actualizar(TiendaJSON tiendaJSON) throws Exception;

    void eliminar(long id) throws Exception;

    List<TiendaJSON> buscar();
}