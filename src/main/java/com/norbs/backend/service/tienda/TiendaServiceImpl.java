package com.norbs.backend.service.tienda;

import com.norbs.backend.domain.tienda.Producto;
import com.norbs.backend.domain.tienda.Tienda;
import com.norbs.backend.repository.base.Repository;
import com.norbs.backend.repository.classes.tables.records.TiendaRecord;
import com.norbs.backend.service.base.BaseService;
import com.norbs.backend.service.compra.CompraService;
import com.norbs.backend.service.util.Util;
import com.norbs.backend.webservice.resources.ProductoJSON;
import com.norbs.backend.webservice.resources.TiendaJSON;
import org.jooq.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Transactional
public class TiendaServiceImpl extends BaseService<Tienda> implements TiendaService {

    @Autowired
    private Repository<Tienda> tiendaRepository;

    @Autowired
    private ProductoService productoService;

    @Autowired
    private CompraService compraService;

    @Override
    @Transactional
    public void insertar(TiendaJSON tiendaJSON) throws Exception {
        Tienda entidad = this.obtenerEntidadPorJSON(tiendaJSON);

        this.validar(entidad);

        this.tiendaRepository.insertar(entidad);

        this.asignarProductos(entidad);

        tiendaJSON.setID(entidad.getId());
    }

    @Override
    public Tienda buscar(long id) {
        return super.buscar(Tienda.class, id);
    }

    @Override
    public TiendaJSON buscarJSON(long id) {

        TiendaJSON tiendaJSON = new TiendaJSON();
        Tienda tienda = this.buscar(id);

        if (tienda != null) {
            tiendaJSON.setID(tienda.getId());
            tiendaJSON.setNombre(tienda.getNombre());
            tiendaJSON.setHorario(tienda.getHorario());
            tiendaJSON.setDireccion(tienda.getDireccion());
            tiendaJSON.setProductos(productoService.buscarPorTienda(id));
        }

        return tiendaJSON;
    }

    @Override
    @Transactional
    public void actualizar(TiendaJSON tiendaJSON) throws Exception {
        Tienda entidad = this.obtenerEntidadPorJSON(tiendaJSON);

        this.validar(entidad);

        this.tiendaRepository.actualizar(entidad);

        this.actualizarProductos(entidad);
    }

    @Override
    @Transactional
    public void eliminar(long id) throws Exception {
        Tienda tienda = this.buscar(id);

        if (tienda == null) {
            throw new Exception("La tienda indicada no existe.");
        }

        if (Util.esNulaOVacia(tienda.getProductos())) {
            tienda.setProductos(this.obtenerProductosTienda(tienda.getId()));
        }

        this.eliminarProductos(tienda.getProductos());

        this.compraService.eliminarComprasTienda(id);

        this.tiendaRepository.eliminar(tienda);
    }

    @Override
    public List<TiendaJSON> buscar() {
        List<TiendaJSON> tiendas = new ArrayList<>();
        Result<TiendaRecord> result = this.tiendaRepository.buscarTiendas();

        if (result != null) {
            for (TiendaRecord tiendaRecord : result) {
                tiendas.add(this.convertirAJSON(tiendaRecord));
            }
        }

        return tiendas;
    }

    @Override
    public void validar(Tienda entidad) throws Exception {
        super.validar(entidad);

        if (Util.esNulaOVacia(entidad.getNombre())) {
            throw new Exception("Indica el nombre de la tienda.");
        }

        if (Util.esNulaOVacia(entidad.getDireccion())) {
            throw new Exception("Ingresa la dirección de la tienda.");
        }

        if (Util.esNulaOVacia(entidad.getHorario())) {
            throw new Exception("Ingresa el horario de la Tienda.");
        }

        if (!Util.esNulaOVacia(entidad.getProductos())) {
            this.verificarDuplicados(entidad.getProductos());
        }
    }

    private void asignarProductos(Tienda entidad) throws Exception {

        if (entidad.getProductos() != null) {
            for (Producto producto : entidad.getProductos()) {
                producto.setTienda(entidad);
                this.productoService.insertar(producto);
            }
        }
    }

    private void actualizarProductos(Tienda entidad) throws Exception {

        if (entidad.getProductos() != null) {
            for (Producto producto : entidad.getProductos()) {
                if (producto.getId() != null && producto.getId() > 0) {
                    this.productoService.actualizar(producto);
                } else {
                    producto.setTienda(entidad);
                    this.productoService.insertar(producto);
                }
            }
        }
    }

    private void eliminarProductos(List<Producto> productos) throws Exception {
        if (!Util.esNulaOVacia(productos)) {
            for (Producto producto : productos) {
                this.productoService.eliminar(producto);
            }
        }
    }

    private TiendaJSON convertirAJSON(TiendaRecord tiendaRecord) {
        TiendaJSON tiendaJSON = new TiendaJSON();
        tiendaJSON.setID(tiendaRecord.getId());
        tiendaJSON.setNombre(tiendaRecord.getNombre());
        tiendaJSON.setHorario(tiendaRecord.getHorario());
        tiendaJSON.setDireccion(tiendaRecord.getDireccion());
        tiendaJSON.setProductos(this.productoService.buscarPorTienda(tiendaJSON.getID()));
        return tiendaJSON;
    }

    private List<Producto> obtenerProductosTienda(long tiendaID) {

        List<Producto> productos = new ArrayList<>();

        List<ProductoJSON> result = this.productoService.buscarPorTienda(tiendaID);

        if (result != null) {
            for (ProductoJSON productoJSON : result) {
                productos.add(this.productoService.buscar(productoJSON.getID()));
            }
        }

        return productos;
    }

    private void verificarDuplicados(List<Producto> productos) throws Exception {
        LinkedList<String> nombres = new LinkedList<>();
        LinkedList<String> codigos = new LinkedList<>();

        for (Producto producto : productos) {
            nombres.add(producto.getNombre());
            codigos.add(producto.getCodigoBarras());
        }

        if (!Util.esNulaOVacia(this.obtenerDuplicados(nombres))) {
            throw new Exception("Los productos deben tener nombres diferentes.");
        }

        if (!Util.esNulaOVacia(this.obtenerDuplicados(codigos))) {
            throw new Exception("Los productos deben tener códigos de barra diferentes.");
        }
    }

    private List<String> obtenerDuplicados(List<String> datos) {
        return datos.stream()
                .collect(Collectors.groupingBy(s -> s))
                .entrySet()
                .stream()
                .filter(e -> e.getValue().size() > 1)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    private Tienda obtenerEntidadPorJSON(TiendaJSON tiendaJSON) {
        Tienda entidad = (tiendaJSON.getID() == null || tiendaJSON.getID() <= 0) ? new Tienda() : this.buscar(tiendaJSON.getID());
        entidad.setDireccion(tiendaJSON.getDireccion());
        entidad.setHorario(tiendaJSON.getHorario());
        entidad.setNombre(tiendaJSON.getNombre());

        if (!Util.esNulaOVacia(tiendaJSON.getProductos())) {
            entidad.setProductos(new ArrayList<>());

            for (ProductoJSON productoJSON : tiendaJSON.getProductos()) {
                Producto producto = (tiendaJSON.getID() == null || tiendaJSON.getID() <= 0) ? new Producto() : this.productoService.buscar(productoJSON.getID());

                if (producto == null) {
                    producto = new Producto();
                }

                if (productoJSON.getTiendaID() != null && productoJSON.getTiendaID() > 0) {
                    producto.setTienda(this.buscar(productoJSON.getTiendaID()));
                }

                producto.setNombre(productoJSON.getNombre());
                producto.setDescripcion(productoJSON.getDescripcion());
                producto.setPrecio(productoJSON.getPrecio());
                producto.setCodigoBarras(productoJSON.getCodigoBarras());
                entidad.getProductos().add(producto);
            }
        }

        return entidad;
    }
}