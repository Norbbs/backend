package com.norbs.backend.service.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import java.util.regex.Pattern;

public class Util {

    public static boolean esNulaOVacia(String cadena) {
        return cadena == null || cadena.trim().isEmpty();
    }

    public static boolean esNulaOVacia(Collection coleccion) {
        return coleccion == null || coleccion.isEmpty();
    }

    public static boolean esEmailValido(String email) {
        return !esNulaOVacia(email) && email.matches("^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$");
    }

    public static boolean esNumerico(String s) {
        return !esNulaOVacia(s) && Pattern.matches("^(0|[1-9][0-9]*)$", s);
    }

    public static boolean equals(BigDecimal a, BigDecimal b) {
        if (a != null && b != null) {
            return a.setScale(2, RoundingMode.UP).compareTo(b.setScale(2, RoundingMode.UP)) == 0;
        } else {
            return Objects.equals(a, b);
        }
    }

    public static boolean esMenor(BigDecimal a, BigDecimal b) {
        return !equals(a, b) && a != null && (b == null || a.compareTo(b) < 0);
    }

    public static boolean esMenorOIgual(BigDecimal a, BigDecimal b) {
        return equals(a, b) || esMenor(a, b);
    }

    public static BigDecimal sumar(BigDecimal a, BigDecimal b) {
        if (a == null) {
            a = BigDecimal.ZERO;
        }

        if (b == null) {
            b = BigDecimal.ZERO;
        }

        return a.add(b);
    }

    public static BigDecimal multiplicar(BigDecimal valor, Integer cantidad) {

        if (valor == null || cantidad == null) {
            return BigDecimal.ZERO;
        }

        return valor.multiply(new BigDecimal(cantidad));
    }

    public static String obtenerFechaFormateada(Timestamp fecha) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        date.setTime(fecha.getTime());
        return dateFormat.format(date);
    }

    public static String obtenerFechaFormateada(Date fecha) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        return dateFormat.format(fecha);
    }
}