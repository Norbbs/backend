package com.norbs.backend.webservice.controller.cliente;

import com.norbs.backend.domain.cliente.Cliente;
import com.norbs.backend.service.cliente.ClienteService;
import com.norbs.backend.webservice.resources.ClienteJSON;
import com.norbs.backend.webservice.response.ExceptionResponse;
import com.norbs.backend.webservice.util.ExceptionUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/rest/cliente")
@Api(value ="/rest/cliente", tags = "cliente")
public class ClienteRestController {

    @Autowired
    private ClienteService clienteService;

    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "CREATED", response = ClienteJSON.class),
            @ApiResponse(code = 400, message = "BAD REQUEST", response = ExceptionResponse.class)
    })
    @RequestMapping(value = "/insertar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> insertar(@RequestBody Cliente cliente) {

        try {
            this.clienteService.insertar(cliente);
            return new ResponseEntity<>(this.buscar(cliente.getId()), HttpStatus.CREATED);
        } catch (Exception e) {
            return ExceptionUtil.generarExcepcion(e);
        }
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = Cliente.class),
    })
    @RequestMapping(value = "/buscar/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> buscar(@PathVariable long id) {
        try {
            return new ResponseEntity<>(this.clienteService.buscar(id), HttpStatus.OK);
        } catch (Exception e) {
            return ExceptionUtil.generarExcepcion(e);
        }
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ClienteJSON.class),
            @ApiResponse(code = 400, message = "BAD REQUEST", response = ExceptionResponse.class)
    })
    @RequestMapping(value = "/actualizar", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> actualizar(@RequestBody Cliente cliente) {

        try {
            this.clienteService.actualizar(cliente);
            return new ResponseEntity<>(this.buscar(cliente.getId()), HttpStatus.OK);
        } catch (Exception e) {
            return ExceptionUtil.generarExcepcion(e);
        }
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "BAD REQUEST", response = ExceptionResponse.class)
    })
    @RequestMapping(value = "/eliminar", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> eliminar(@RequestParam long id) {

        try {
            this.clienteService.eliminar(id);
            return new ResponseEntity<>("El cliente ha sido eliminado de forma exitosa.", HttpStatus.OK);
        } catch (Exception e) {
            return ExceptionUtil.generarExcepcion(e);
        }
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ClienteJSON.class),
            @ApiResponse(code = 400, message = "BAD REQUEST", response = ExceptionResponse.class)
    })
    @RequestMapping(value = "/iniciar-sesion", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> iniciarSesion(@RequestParam String nombreUsuario, @RequestParam String clave) {

        try {
            return new ResponseEntity<>(this.clienteService.iniciarSesion(nombreUsuario, clave), HttpStatus.OK);
        } catch (Exception e) {
            return ExceptionUtil.generarExcepcion(e);
        }
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ClienteJSON.class),
    })
    @RequestMapping(value = "/buscar", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> buscar() {
        try {
            return new ResponseEntity<>(this.clienteService.buscar(), HttpStatus.OK);
        } catch (Exception e) {
            return ExceptionUtil.generarExcepcion(e);
        }
    }
}
