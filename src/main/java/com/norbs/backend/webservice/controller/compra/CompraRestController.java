package com.norbs.backend.webservice.controller.compra;

import com.norbs.backend.service.compra.CompraService;
import com.norbs.backend.webservice.resources.CarritoJSON;
import com.norbs.backend.webservice.resources.ClienteJSON;
import com.norbs.backend.webservice.resources.CompraJSON;
import com.norbs.backend.webservice.response.ExceptionResponse;
import com.norbs.backend.webservice.util.ExceptionUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/rest/compra")
@Api(value ="/rest/compra", tags = "compra")
public class CompraRestController {

    @Autowired
    private CompraService compraService;

    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "CREATED", response = CompraJSON.class),
            @ApiResponse(code = 400, message = "BAD REQUEST", response = ExceptionResponse.class)
    })
    @RequestMapping(value = "/procesar-compra", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> procesar(@RequestBody CarritoJSON carritoCompras) {

        try {
            return new ResponseEntity<>(this.compraService.procesarCompra(carritoCompras), HttpStatus.CREATED);
        } catch (Exception e) {
            return ExceptionUtil.generarExcepcion(e);
        }
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = CompraJSON.class),
    })
    @RequestMapping(value = "/consultar-compras-cliente/{clienteID}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> buscar(@PathVariable long clienteID) {
        try {
            return new ResponseEntity<>(this.compraService.consultarComprasCliente(clienteID), HttpStatus.OK);
        } catch (Exception e) {
            return ExceptionUtil.generarExcepcion(e);
        }
    }
}
