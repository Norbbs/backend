package com.norbs.backend.webservice.controller.tienda;

import com.norbs.backend.service.tienda.ProductoService;
import com.norbs.backend.webservice.resources.ProductoJSON;
import com.norbs.backend.webservice.response.ExceptionResponse;
import com.norbs.backend.webservice.util.ExceptionUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/rest/producto")
@Api(value ="/rest/producto", tags = "producto")
public class ProductoRestController {

    @Autowired
    private ProductoService productoService;

    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "CREATED", response = ProductoJSON.class),
            @ApiResponse(code = 400, message = "BAD REQUEST", response = ExceptionResponse.class)
    })
    @RequestMapping(value = "/insertar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> insertar(@RequestBody ProductoJSON productoJSON) {

        try {
            this.productoService.insertar(productoJSON);
            return new ResponseEntity<>(this.buscar(productoJSON.getID()), HttpStatus.CREATED);
        } catch (Exception e) {
            return ExceptionUtil.generarExcepcion(e);
        }
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ProductoJSON.class),
    })
    @RequestMapping(value = "/buscar/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> buscar(@PathVariable long id) {
        try {
            return new ResponseEntity<>(this.productoService.buscarJSON(id), HttpStatus.OK);
        } catch (Exception e) {
            return ExceptionUtil.generarExcepcion(e);
        }
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ProductoJSON.class),
            @ApiResponse(code = 400, message = "BAD REQUEST", response = ExceptionResponse.class)
    })
    @RequestMapping(value = "/actualizar", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> actualizar(@RequestBody ProductoJSON productoJSON) {

        try {
            this.productoService.actualizar(productoJSON);
            return new ResponseEntity<>(this.buscar(productoJSON.getID()), HttpStatus.OK);
        } catch (Exception e) {
            return ExceptionUtil.generarExcepcion(e);
        }
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "BAD REQUEST", response = ExceptionResponse.class)
    })
    @RequestMapping(value = "/eliminar", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> eliminar(@RequestParam long id) {

        try {
            this.productoService.eliminar(id);
            return new ResponseEntity<>("El producto ha sido eliminado de forma exitosa.", HttpStatus.OK);
        } catch (Exception e) {
            return ExceptionUtil.generarExcepcion(e);
        }
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ProductoJSON.class),
    })
    @RequestMapping(value = "/buscar", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> buscar() {
        try {
            return new ResponseEntity<>(this.productoService.buscar(), HttpStatus.OK);
        } catch (Exception e) {
            return ExceptionUtil.generarExcepcion(e);
        }
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = ProductoJSON.class),
    })
    @RequestMapping(value = "/buscar-por-tienda/{tiendaID}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> buscarPorTienda(@PathVariable long tiendaID) {
        try {
            return new ResponseEntity<>(this.productoService.buscarPorTienda(tiendaID), HttpStatus.OK);
        } catch (Exception e) {
            return ExceptionUtil.generarExcepcion(e);
        }
    }
}
