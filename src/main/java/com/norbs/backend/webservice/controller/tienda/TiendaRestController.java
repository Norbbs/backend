package com.norbs.backend.webservice.controller.tienda;

import com.norbs.backend.domain.tienda.Tienda;
import com.norbs.backend.service.tienda.TiendaService;
import com.norbs.backend.webservice.resources.TiendaJSON;
import com.norbs.backend.webservice.response.ExceptionResponse;
import com.norbs.backend.webservice.util.ExceptionUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/rest/tienda")
@Api(value ="/rest/tienda", tags = "tienda")
public class TiendaRestController {

    @Autowired
    private TiendaService tiendaService;

    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "CREATED", response = TiendaJSON.class),
            @ApiResponse(code = 400, message = "BAD REQUEST", response = ExceptionResponse.class)
    })
    @RequestMapping(value = "/insertar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> insertar(@RequestBody TiendaJSON tiendaJSON) {

        try {
            this.tiendaService.insertar(tiendaJSON);
            return new ResponseEntity<>(this.buscar(tiendaJSON.getID()), HttpStatus.CREATED);
        } catch (Exception e) {
            return ExceptionUtil.generarExcepcion(e);
        }
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = TiendaJSON.class),
    })
    @RequestMapping(value = "/buscar/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> buscar(@PathVariable long id) {
        try {
            return new ResponseEntity<>(this.tiendaService.buscarJSON(id), HttpStatus.OK);
        } catch (Exception e) {
            return ExceptionUtil.generarExcepcion(e);
        }
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = TiendaJSON.class),
            @ApiResponse(code = 400, message = "BAD REQUEST", response = ExceptionResponse.class)
    })
    @RequestMapping(value = "/actualizar", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> actualizar(@RequestBody TiendaJSON tiendaJSON) {

        try {
            this.tiendaService.actualizar(tiendaJSON);
            return new ResponseEntity<>(this.buscar(tiendaJSON.getID()), HttpStatus.OK);
        } catch (Exception e) {
            return ExceptionUtil.generarExcepcion(e);
        }
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "BAD REQUEST", response = ExceptionResponse.class)
    })
    @RequestMapping(value = "/eliminar", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> eliminar(@RequestParam long id) {

        try {
            this.tiendaService.eliminar(id);
            return new ResponseEntity<>("La tienda ha sido eliminada de forma exitosa.", HttpStatus.OK);
        } catch (Exception e) {
            return ExceptionUtil.generarExcepcion(e);
        }
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok", response = TiendaJSON.class),
    })
    @RequestMapping(value = "/buscar", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> buscar() {
        try {
            return new ResponseEntity<>(this.tiendaService.buscar(), HttpStatus.OK);
        } catch (Exception e) {
            return ExceptionUtil.generarExcepcion(e);
        }
    }

}
