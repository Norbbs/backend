package com.norbs.backend.webservice.resources;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel(value = "Carrito")
public class CarritoJSON {

    @ApiModelProperty(name = "clienteID", dataType = "Long", position = 1)
    private Long clienteID;

    @ApiModelProperty(name = "items", required = true, dataType = "BigDecimal", position = 2)
    private List<ItemCarritoJSON> items;

    public Long getClienteID() {
        return clienteID;
    }

    public void setClienteID(Long clienteID) {
        this.clienteID = clienteID;
    }

    public List<ItemCarritoJSON> getItems() {
        return items;
    }

    public void setItems(List<ItemCarritoJSON> items) {
        this.items = items;
    }
}