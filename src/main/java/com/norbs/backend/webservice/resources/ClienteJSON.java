package com.norbs.backend.webservice.resources;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Cliente")
public class ClienteJSON {

    @ApiModelProperty(name = "ID", dataType = "java.lang.Long", position = 1)
    private Long ID;

    @ApiModelProperty(name = "nombre", required = true, dataType = "String", position = 2)
    private String nombre;

    @ApiModelProperty(name = "apellido", required = true, dataType = "String", position = 3)
    private String apellido;

    @ApiModelProperty(name = "edad", notes = "Debe ser mayor a cero.", required = true, dataType = "Short", position = 4)
    private short edad;

    @ApiModelProperty(name = "documento", notes = "No deben existir documentos iguales.", required = true, dataType = "String", position = 5)
    private String documento;

    @ApiModelProperty(name = "correoElectronico", notes = "No deben existir correos electrónicos iguales.", required = true, dataType = "String", position = 6)
    private String correoElectronico;

    @ApiModelProperty(name = "usuario", notes = "No deben existir usuarios iguales.", required = true, dataType = "String", position = 7)
    private String usuario;

    @ApiModelProperty(name = "correoElectronico", notes = "Debe tener mínimo 6 caracteres.", required = true, dataType = "String", position = 8)
    private String clave;

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public short getEdad() {
        return edad;
    }

    public void setEdad(short edad) {
        this.edad = edad;
    }
}
