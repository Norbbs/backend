package com.norbs.backend.webservice.resources;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.List;

@ApiModel(value = "Compra")
public class CompraJSON {

    @ApiModelProperty(name = "ID", dataType = "Long", position = 1)
    private Long ID;

    @ApiModelProperty(name = "fecha", notes = "Fecha en que se realizó la compra.", required = true, dataType = "String", position = 2)
    private String fecha;

    @ApiModelProperty(name = "documentoCliente", required = true, dataType = "String", position = 3)
    private String documentoCliente;

    @ApiModelProperty(name = "nombreCliente", required = true, dataType = "String", position = 4)
    private String nombreCliente;

    @ApiModelProperty(name = "nombreTienda", required = true, dataType = "String", position = 5)
    private String nombreTienda;

    @ApiModelProperty(name = "precioTotal", required = true, dataType = "BigDecimal", position = 6)
    private BigDecimal precioTotal;

    @ApiModelProperty(name = "items", required = true, dataType = "BigDecimal", position = 7)
    private List<ItemCompraJSON> items;

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDocumentoCliente() {
        return documentoCliente;
    }

    public void setDocumentoCliente(String documentoCliente) {
        this.documentoCliente = documentoCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getNombreTienda() {
        return nombreTienda;
    }

    public void setNombreTienda(String nombreTienda) {
        this.nombreTienda = nombreTienda;
    }

    public BigDecimal getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(BigDecimal precioTotal) {
        this.precioTotal = precioTotal;
    }

    public List<ItemCompraJSON> getItems() {
        return items;
    }

    public void setItems(List<ItemCompraJSON> items) {
        this.items = items;
    }
}