package com.norbs.backend.webservice.resources;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

@ApiModel(value = "ItemCarrito")
public class ItemCarritoJSON {

    @ApiModelProperty(name = "productoID", dataType = "Long", position = 1)
    private Long productoID;

    @ApiModelProperty(name = "cantidad", required = true, dataType = "BigDecimal", position = 2)
    private BigDecimal cantidad;

    public Long getProductoID() {
        return productoID;
    }

    public void setProductoID(Long productoID) {
        this.productoID = productoID;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }
}
