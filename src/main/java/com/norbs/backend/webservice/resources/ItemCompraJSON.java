package com.norbs.backend.webservice.resources;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

@ApiModel(value = "ItemCompra")
public class ItemCompraJSON {

    @ApiModelProperty(name = "ID", dataType = "Long", position = 1)
    private Long ID;

    @ApiModelProperty(name = "compraID", dataType = "Long", position = 2)
    private Long compraID;

    @ApiModelProperty(name = "nombreProducto", required = true, dataType = "String", position = 3)
    private String nombreProducto;

    @ApiModelProperty(name = "precioUnitario", required = true, dataType = "BigDecimal", position = 4)
    private BigDecimal precioUnitario;

    @ApiModelProperty(name = "cantidad", required = true, dataType = "BigDecimal", position = 5)
    private BigDecimal cantidad;

    @ApiModelProperty(name = "precioTotal", required = true, dataType = "BigDecimal", position = 5)
    private BigDecimal precioTotal;

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public Long getCompraID() {
        return compraID;
    }

    public void setCompraID(Long compraID) {
        this.compraID = compraID;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public BigDecimal getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(BigDecimal precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(BigDecimal precioTotal) {
        this.precioTotal = precioTotal;
    }
}