package com.norbs.backend.webservice.resources;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

public class ProductoJSON {

    @ApiModelProperty(name = "ID", dataType = "Long", position = 1)
    private Long ID;

    @ApiModelProperty(name = "tiendaID", dataType = "Long", position = 2)
    private Long tiendaID;

    @ApiModelProperty(name = "nombre", notes = "No deben existir productos con el mismo nombre.", required = true, dataType = "String", position = 2)
    private String nombre;

    @ApiModelProperty(name = "descripcion", required = true, dataType = "String", position = 3)
    private String descripcion;

    @ApiModelProperty(name = "precio", notes = "El precio del producto debe ser mayor a cero.", required = true, dataType = "BigDecimal", position = 4)
    private BigDecimal precio;

    @ApiModelProperty(name = "codigoBarras", notes = "No deben existir productos con el mismo código de barras.", required = true, dataType = "String", position = 5)
    private String codigoBarras;

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public Long getTiendaID() {
        return tiendaID;
    }

    public void setTiendaID(Long tiendaID) {
        this.tiendaID = tiendaID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public String getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }
}
