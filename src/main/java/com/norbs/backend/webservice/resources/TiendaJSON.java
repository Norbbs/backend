package com.norbs.backend.webservice.resources;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class TiendaJSON {

    @ApiModelProperty(name = "ID", dataType = "Long", position = 1)
    private Long ID;

    @ApiModelProperty(name = "nombre", required = true, dataType = "String", position = 2)
    private String nombre;

    @ApiModelProperty(name = "horario", required = true, dataType = "String", position = 3)
    private String horario;

    @ApiModelProperty(name = "direccion", required = true, dataType = "String", position = 4)
    private String direccion;

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    private List<ProductoJSON> productos;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public List<ProductoJSON> getProductos() {
        return productos;
    }

    public void setProductos(List<ProductoJSON> productos) {
        this.productos = productos;
    }
}