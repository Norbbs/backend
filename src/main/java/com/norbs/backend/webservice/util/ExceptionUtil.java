package com.norbs.backend.webservice.util;

import com.norbs.backend.webservice.response.ExceptionResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ExceptionUtil {

    public static ResponseEntity<?> generarExcepcion(Exception e) {
        return new ResponseEntity<>(new ExceptionResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
    }
}